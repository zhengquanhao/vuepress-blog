const nav = require("./nav");
const sidebar = require("./sidebar");

module.exports = {
    title: "Ron's Blog",
    description: "A Personal Blog",
    base: process.env.NODE_ENV === "production" ? "./" : "/",
    port: "8000",
    dest: "./dist",
    head: [
        ["link", { rel: "icon", href: "/favicon.ico" }],
        ["link", { rel: "stylesheet", href: "/css/style.css" }],
        ["script", { rel: "stylesheet", type: "text/javascript", src: "/js/main.js" }]
    ],
    markdown: {
      lineNumbers: true
    },
    themeConfig: {
        nav,
        sidebar,
        sidebarDepth: 2,
        lastUpdated: "Last Updated"
    }
};