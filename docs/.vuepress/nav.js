module.exports = [
    {
        text: "技术汇总",
        items: [ // 子标题
            {
                text: "入门及工具",
                items: [
                    {
                        text: "前端入门路线",
                    },
                    {
                        text: "必备工具介绍",
                    },
                ]
            },
            {
                text: "前端三大件",
                items: [
                    {
                        text: "HTML",
                        link: "/Html/"
                    },
                    {
                        text: "CSS",
                        link: "/Css/"
                    },
                    {
                        text: "JavaScript",
                        link: "/JS/"
                    }
                ]
            },
            {
                text: "JS高级知识",
                items: [
                    {
                        text: "ES6"
                    },
                    {
                        text: "Node"
                    }
                ]
            },
            {
                text: "三大框架",
                items: [ // 子标题
                    { text: "React", link: "https://zh-hans.reactjs.org/"}, // 外部链接
                    { text: "Vue", link: 'https://cn.vuejs.org/' },
                    { text: "Angular", link: "https://angular.cn/" },
                    { text: "Vue3.0"}
                ]
            },
            {
                text: "构建工具",
                items: [
                    {
                        text: "Webpack"
                    },
                    {
                        text: "Vite"
                    }
                ]
            },
            {
                text: "永无止境",
                items: [
                    {
                        text: "TypeScript"
                    },
                    {
                        text: "React Native"
                    }
                ]
            },
        ]
    },
    {
        text: "面试题目",
        link: "/interview/" // 内部链接 以docs为根目录
    },
    {
        text: "工具搭建",
        link: "/Tools/" // 内部链接 以docs为根目录
    },
    {
        text: "GitLab",
        link: "https://gitlab.com/zhengquanhao/vuepress-blog"
    }
]