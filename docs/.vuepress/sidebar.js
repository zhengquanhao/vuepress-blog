module.exports = {
    "/Tools/": [
        {
            title: "NPM",
            path: "/Tools/npm/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "手写一个 NPM 包",
                    path: "/Tools/npm/01_WriteAnNpmPackage/"
                },
                {
                    title: "发布自己的 NPM 包",
                    path: "/Tools/npm/02_PublishNpmPackage/"
                }
            ]
        },
        {
            title: "Vite",
            path: "/Tools/vite/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "Vite项目搭建",
                    path: "/Tools/vite/01_ViteProjectConstruction/"
                }
            ]
        },
        {
            title: "Vue",
            path: "/Tools/vue/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "Vue项目配置在局域网下访问",
                    path: "/Tools/vue/01_AccessUnderLAN/"
                }
            ]
        },
    ],
    "/JS/": [
        {
            title: "简介",
            path: "/JS/01_Introduction/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "JavaScript 简介",
                    path: "/JS/01_Introduction/01_IntroductionToJavaScript/"
                },
                {
                    title: "代码编辑器",
                    path: "/JS/01_Introduction/02_CodeEditor/"
                },
                {
                    title: "开发者控制台",
                    path: "/JS/01_Introduction/03_DeveloperConsole/"
                }
            ]
        },
        {
            title: "JavaScript 基础知识",
            path: "/JS/02_BasicKnowledge/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "Hello, World!",
                    path: "/JS/02_BasicKnowledge/01_HelloWorld/"
                },
                {
                    title: "代码结构",
                    path: "/JS/02_BasicKnowledge/02_CodeStructure/"
                }
            ]
        },
        {
            title: "代码质量",
            path: "/JS/03_CodeQuality/",
            collapsable: false,
            sidebarDepth: 1,
            children: [
                {
                    title: "在 Chrome 中调试",
                    path: "/JS/03_CodeQuality/01_Debugging/"
                }
            ]
        }
    ]
}