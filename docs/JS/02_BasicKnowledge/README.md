简介
---

让我们来一起学习 `JavaScript` 脚本构建的基础知识。

- [Hello World](/JS/02_BasicKnowledge/01_HelloWorld/)

- [代码结构](/JS/02_BasicKnowledge/02_CodeStructure/)