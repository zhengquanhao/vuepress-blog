现代 JavaScript 教程
---

以最新的 JavaScript 标准为基准。通过简单但足够详细的内容，为你讲解从基础到高阶的 JavaScript 相关知识。

- [JavaScript 简介](/JS/01_Introduction/)

- [JavaScript 基础知识](/JS/02_BasicKnowledge/)