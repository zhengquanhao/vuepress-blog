---
home: true
lang: zh-CN
heroText: Ron's Blog
tagline: 一个汇总前端知识的个人博客
heroImage: /logo.jpg
actionText: Let's go →
actionLink: /index/
features:
- title: For The Record
  details: 养成良好的习惯，记录日常所得。
- title: For Growth
  details: 每天进步一点点，脚踏实地，日积月累。
- title: For Giving Back
  details: 文档开源供，为自己及身边人提供便捷。
footer: Copyright © 2021-present Ron
---