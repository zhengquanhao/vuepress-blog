Vue开发中遇到的问题整理
---

## 一、如何在data/computed里调用methods中的方法

以data为例：

```vue
data() {
    let self = this  // 加上这一句就OK了
    return {
        list: [
            {
                label: "text",
                num: 10,
                cb () {
                    self.goTo();
                }
            }
        ]
    };
}
```