手写一个Vue组件发布到npm
---

## 前言

我们为什么要写个组件上传到 `npm` 镜像上呢，我们肯定遇到过这样一个场景，项目中有很多地方与某个功能相似，你想到的肯定是把该功能封装成 `Component` 组件，后续方便我们调用。但是过了一段时间，你的Leader让你去开发另一个项目，结果你在哪个项目中又看见了类似的功能，你这时会怎么做?   你也可以使用 `Ctrl + c + v` 大法，拿过来上一个项目封装好的代码，但是如果需求有些变动，你得维护两套项目的代码，甚至以后更多的项目....，这时你就可以封装一个功能上传到你们公司内网的 `npm` 上(或者自己的账号上)，这样每次遇到类似的功能直接 `npm install` 安装并`import` 导入进来使用就可以，需求有变动时完全可以改动一处代码。


## 开始开发


### 编辑 `package.json` 文件

这里说明一下相关属性

- name：即项目名
- version：即包的版本
- description：描述信息
- main：指定入口文件
- scripts：脚本命令
- repository：源码仓库地址
- keywords：关键词
- author：作者信息
- license：版权类型
- bugs：bug反馈
- homepage：项目主页

必须包含的项目有：name 和 version

最终`package.json`文件：


除了上面列出的以外，还会有 dependencies 跟 devDependencies ，分别表示项目的依赖包以及项目开发中用到的依赖包。这里不写出来，是因为，当我们要添加依赖时，通过 npm install --save 或 npm install --save-dev 来安装，所依赖的包及版本信息都会添加到上面对应的属性中，基本无需手动编辑。

https://juejin.cn/post/6844904030976606216#heading-13