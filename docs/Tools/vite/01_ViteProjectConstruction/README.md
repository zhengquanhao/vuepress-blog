搭建第一个 Vite 项目
---

## 一、前提环境

使用`Vite` 需要 `Node.js` 版本 `>= 12.0.0`

查看Node版本指令

```shell
node -v
或
node --version
```
## 二、搭建Vite项目

使用 NPM:

```shell
npm init vite@latest
```

使用 Yarn:

```shell
yarn create vite
```

使用 PNPM:

```shell
pnpm create vite
```

然后按照提示操作即可，我们在这里选择 `vue` 框架。


随后运行一下命令进入并启动项目

```shell
cd vite-project
npm i
npm run dev
```

> 细心的小伙伴可能发现搭建`vite`项目时间特别短，因为`webpack`搭建项目时帮我们字段安装了依赖而`vite`需要手动安装。


## 三、基本配置

在`vite.config.js`进行基本配置，包含本地服务器配置，别名配置，反向代理。

```javascript
import { defineConfig } from "vite"
import { resolve } from "path"
import vue from "@vitejs/plugin-vue"

function pathResolve(dir) {
    return resolve(__dirname, ".", dir);
}


export default defineConfig({
    plugins:[vue()], // 配置需要使用的插件列表，这里将vue添加进去
    resolve: {
        alias: {
          "/@": pathResolve("src"), // 这里是将src目录配置别名为 /@ 方便在项目中导入src目录下的文件
        }
    },
    // 本地运行配置，及反向代理配置
    server: {
        host: "127.0.0.1", // 默认是 localhost
        port: "8000", // 默认是 3000 端口
        cors: true, // 默认启用并允许任何源
        open: true, // 浏览器自动打开
        //反向代理配置，注意rewrite写法，开始没看文档在这里踩了坑
        proxy: {
            "/api": {
                target: "http://目标地址",   //代理接口
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, "")
          }
        }
    }
})
```


## 四、使用路由


### 4.1 安装vue-router依赖

```shell
npm install vue-router@next -S
```

### 4.2 路由文件配置

新建`router`文件夹`index.js`文件并写入路由配置。

```javascript
import { createRouter, createWebHistory } from "vue-router"

// 开启历史模式
// vue2中使用 mode: history 实现
const routerHistory = createWebHistory();

const router = createRouter({
    history: routerHistory,
    routes: [
        {
            path: "/",
            redirect: "/home"
        },
        {
            path: "/home",
            name: "home",
            component: () => import("@/views/Home.vue")
        },
        {
            path: "/about",
            name: "about",
            component: () => import("@/views/About.vue")
        }
    ]
})

export default router;
```

> 此处省略搭建`Home.vue`，`About.vue`文件。


### 4.3 路由引用

在入口文件 `main.js` 中引用路由：

```javascript
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

const app = createApp(App)
app.use(router)
app.mount('#app')
```

### 4.4 路由容器

在`App.vue`中加入路由容器：

```vue
<router-view></router-view>
```

### 4.5 路由跳转

在 `About.vue` 文件尝试下路由跳转：

```vue
<template>
    <div class="about">
        <p>About</p>
        <button @click="toHome">toHome</button>
    </div>
</template>
<script>
import { useRouter } from 'vue-router'
export default {
  setup () {
    const router = useRouter()
    const toHome = (() => {
      router.push({
        name: 'home'
      })
    })
    return {
      toHome
    }
  },
}
</script>
<style  scoped>
</style>
```

### 4.6 遇到的坑

当使用`npm install vue-router`方式安装，且使用 `createRouter` 函数式可能遇到一下报错，将vue-router卸载，并使用`npm install vue-router@next -S`安装问题解决。

```shell
The requested module '/node_modules/.vite/vue-router.js?v=8b9a6bbd' does not provide an export named 'createRouter'
```





## 添加环境变量
根目录下新建`.env`, `.env.pre`, `.env.production`, `.emv.test`，写入不同环境下的环境变量信息，这里我们配置一下几条，可自行添加。

```shell
NODE_ENV = 'development'
BASE_URL = '/'
VUE_APP_BASE_API = '/api'
VUE_APP_TITLE = 'dev'
```

环境变量使用方式举例： `process.env.NODE_ENV`

## 更多参考

项目GitLab地址[vue3-vite-template](https://gitlab.com/zhengquanhao/vue3-vite-template)

更多学习请参见[vite官方文档](https://cn.vitejs.dev/)