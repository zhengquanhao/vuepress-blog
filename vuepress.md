VuePress-Blog搭建
---

## 一、概述

`VuePress` 是一个以 `Markdown` 为中心的静态网站生成器。你可以使用 Markdown 来书写内容（如文档、博客等），然后 `VuePress` 会帮助你生成一个静态网站来展示它们。
> 详情请转到[VuePress官网](https://vuepress.vuejs.org/)

### 前置环境安装

- 需要`node`环境和`npm支持`
- `VuePress` 需要 `Node.js` >= 8.6
- node安装请转到[Node官网](https://nodejs.org/)

### VuePress概述

- `Vue驱动` 的静态网站生成器
- 基于 `Markdown` 语法生成页面
- 可自定义和扩展样式
- 可以发布至 `Github`


## 二、安装VuePress

1.创建并进入一个新目录

```shell
mkdir vuepress-blog && cd vuepress-blog
```

2.使用你喜欢的包管理器进行初始化

```shell
npm init -y
```

3.将 `VuePress` 安装为本地依赖 (我们已经不再推荐全局安装 VuePress)

```shell
npm install -D vuepress
```

> 注意: 如果你的现有项目依赖了 webpack 3.x，我们推荐使用 Yarn (opens new window)而不是 npm 来安装 VuePress。因为在这种情形下， npm 会生成错误的依赖树。

4.创建你的第一篇文档

```shell
mkdir docs && cd docs
touch README.md
```

生成 `docs/README.md`文件，该 `README.md` 会作为你系统的首页，从此可编写你的第一篇文档了。

5.在 package.json 中添加一些 scripts

这一步骤是可选的，但推荐完成。在下文中，我们会默认这些 `scripts` 已经被添加。

```javascript
{
  "scripts": {
    "dev": "vuepress dev docs",
    "build": "vuepress build docs"
  }
}
```

6.启动本地服务器

```shell
npm run dev
```

VuePress 会在 http://localhost:8080 启动一个热重载的开发服务器。

### 三、配置首页（默认主题提供）

1.在`docs/README.md`进行如下配置

```markdown
---
home: true
lang: zh-CN
heroText: JavaScript Blog
tagline: 一个汇总JavaScript基础知识的博客
heroImage: /logo.jpg
actionText: Let's go →
actionLink: /index/
features:
- title: For The Record
  details: 养成良好的习惯，记录日常所得。
- title: For Growth
  details: 每天进步一点点，脚踏实地，日积月累。
- title: For Giving Back
  details: 文档开源供，为自己及身边人提供便捷。
footer: Copyright © 2021-present Ron
---
```

> 书写注意：开头和结尾的`---`不能漏。


2.首页配置说明

在首页的README配置文件中，我们填写了一些配置，下面我们将详细描述每一个配置的具体含义

- home:true：标记此页面是否为首页
- lang:zh-CN：表示本页面的语言为zh-CN(简体中文)
- heroText: 首页的标题内容
- tagline: 首页的子标题内容
- heroImage: 首页的标题图片，其中全路径为 `docs/.vuepress/public/logo.jpg`，默认去public目录下找静态资源
- actionText: 首页跳转按钮的内容
- actionLink: 首页跳转按钮挑战的路径，其中全路径为 `docs/index/readme.md`，默认readme命名的文件可以省略不写链接的后面内容，省略后的链接
- features: 表明首页的特征，固定的格式为 `title + details` ，以三栏流式布局的方式展示
- footer: 为底部内容，与普通的网页一样，我们可以在footer里面写版权信息

3.首页效果展示：

![home](./md-images/home.png)

> https://juejin.cn/post/6844903842375532558

### 四、核心配置

1.在docs目录下创建 `.vuepress` 目录。

```shell
cd docs
mkdir .vuepress
```

2.新建总配置文件 `config.js`

```shell
cd .vuepress
touch config.js
```

`config.js` 是整个项目的核心配置文件，所有菜单、栏目相关的配置都在该文件中。

3.在 `config.js` 中加入内容。

```javascript
module.exports = {
    title: "JavaScript Blog",
    description: "A JavaScript Blog",
    base: "/",
    port: "8000",
    dest: "/dist",
    head: [
      ["link", { rel: "icon", href: "/favicon.ico" }],
    ],
    markdown: {
      lineNumbers: true
    },
    themeConfig: {
        nav,
        sidebar,
        sidebarDepth: 2,
        lastUpdated: "Last Updated"
    }
};
```

4.`config.js` 核心配置说明

- title: 网站标题
- description: 网站描述，书写后网页上不展示，会在网页`<head>`标签内增加`<meta name="description" content="A JavaScript Blog">`
- base: 站点的根路径，`base` 属性的默认值是 `/`
- port: 项目启动端口号
- dest: 文件输出目录，将文件输出在项目根目录下的 `dist` 文件夹中
- head: 将内容注入到当前页面的 `<head>` 中的标签
- `href: "/favicon.ico"`: 如果需要自定义网站 `ico图标`，默认在 `docs/.vuepress/public` 路径下添加 `favicon.ico`
- markdown.lineNumbers: 代码块显示行号
- themeConfig.nav: 导航栏
- themeConfig.sidebar: 侧边栏
- themeConfig.sidebarDepth: `sidebarDepth` 为2将同时提取markdown中`h2` 和 `h3` 标题，显示在侧边栏上。
- themeConfig.lastUpdated: 文档更新时间：每个文件git最后提交的时间

> 注意: 修改 `config.js` 文件后，需要重启项目页面才会生效。


### 五、导航栏配置

1.在`docs/.vuepress`下新建`nav.js`

```shell
touch nav.js
```

2.在 `nav.js` 中加入内容。

```javascript
module.exports = [
    {
        text: "三大框架",
        items: [ // 子标题
            { text: "React", link: "https://zh-hans.reactjs.org/"}, // 外部链接
            { text: "Vue", link: 'https://cn.vuejs.org/' },
            { text: "Angular", link: "https://angular.cn/" }
        ]
    },
    {
        text: "面试题目",
        link: "/interview/" // 内部链接 以docs为根目录
    },
    {
        text: "GitLab",
        link: "https://gitlab.com/zhengquanhao/vuepress-blog"
    }
]
```

3.在 `config.js` 中引入导航栏配置并使用。

```javascript
const nav = require("./nav");

module.exports = {
    ...
    themeConfig: {
        nav
        ...
    }
};
```

4.在 `docs` 目录下新建 `interview` 文件夹，并在 `interview` 文件夹下创建 `README.md`

```shell
mkdir interview && cd interview
touch README.md
```

> 注意: `README.md` 文件是必需的，因为 `/interview/` 实际指的是 `/docs/interview/README.md` 文件

5.导航栏配置说明

- text: 导航栏标题
- items: 子导航
- link: 跳转链接；可以为内部链接也可以为外部链接，使用外部链接时会在右侧显示外链图标

6.导航栏展示效果

![nav](./md-images/nav.png)

### 六、侧边栏配置

1.在`docs/.vuepress`下新建`sidebar.js`

```shell
touch sidebar.js
```

2.在 `sidebar.js` 中加入内容。

```javascript
module.exports = [
    {
        title: '简介',
        path: '/01_Introduction/',
        collapsable: false,
        sidebarDepth: 1,
        children: [
            {
                title: "JavaScript 简介",
                path: "/01_Introduction/01_IntroductionToJavaScript/"
            },
            {
                title: "代码编辑器",
                path: "/01_Introduction/02_CodeEditor/"
            },
            {
                title: "开发者控制台",
                path: "/01_Introduction/03_DeveloperConsole/"
            }
        ]
    },
    {
        title: "JavaScript 基础知识",
        path: '/02_BasicKnowledge/',
        collapsable: true,
        sidebarDepth: 1,
        children: [
            {
                title: "Hello, World!",
                path: "/02_BasicKnowledge/01_HelloWorld/"
            },
            {
                title: "代码结构",
                path: "/02_BasicKnowledge/02_CodeStructure/"
            }
        ]
    }
]
```
3.在 `config.js` 中引入导航栏配置并使用。

```javascript
const nav = require("./sidebar");

module.exports = {
    ...
    themeConfig: {
        sidebar
        ...
    }
};
```

4.在 `docs` 目录对应的文件夹下创建 `README.md`

5.项目目录结构

![directory](./md-images/directory.png)

6.侧边栏配置说明

- title: 侧边栏标题（必填）
- path: 标题的跳转链接，不写则表示不可点击跳转
- collapsable: 是否可折叠，默认值是 true
- sidebarDepth: 当前菜单栏目的深度，根据深度将`h1~h6`展示在菜单栏中，默认值是1
- children: 子侧边栏信息

7.侧边栏展示效果

![sidebar](./md-images/sidebar.png)

### 七、静态资源

静态资源是重要的一部分，比如图片、css、js如何在项目中引入让我们一起学习一下。

vuepress默认程序的图片目录是`docs/.vuepress/public`下。

- 图片

比如我们在首页显示指定显示图片，需要将首页配置更改如下。

```shell
heroImage: /logo.jpg
```

即 `/logo.jpg` 就是指 `/docs/.vuepress/public/logo.jpg`。

- css/js

1.css与js默认目录与图片一样。

2.创建css/js文件

```shell
cd public
mkdir css && cd css
touch style.css
```

```shell
cd public
mkdir js && cd js
touch main.js
```

并加入内容:

```css
#app .navbar .home-link span:before {
    display: inline-block;
    content: "";
    width: 1.5rem;
    height: 1.5rem;
    background: url("../logo.jpg") no-repeat;
    background-size: 100% 100%;
    vertical-align: sub;
    margin-right: 0.5rem;
}
```

```javascript
console.log("插入js");
```

3.使用自定义的css/js

想要使用自定义的css/js，需要在 `config.js` 下 `head` 属性增加配置。

```javascript
...
head: [
    ["link", { rel: "stylesheet", href: "/css/style.css" }],
    ["script", { rel: "stylesheet", type: "text/javascript", src: "/js/main.js" }]
]
```
配置完成重启后便可在 `界面/控制台` 看到对应效果。

### 八、打包部署

1.根据环境变量调整路径
更改`config.js`中的路径配置

```javascript
...
base: process.env.NODE_ENV === "production" ? "./" : "/",
```

2.更改路由方式为哈希模式

然后打开项目下的 `node_modules\@vuepress\core\lib\client` 下的 `app.js` 文件，把 `mode: 'history'`, 注释掉就行了（让它默认为 hash 模式）。

3.打包项目

```shell
npm run build
```

将打包好的静态资源用自己喜欢的方式部署就完成啦！
### 九、其他

如果你想配置版心、颜色等参数请参考[这里](https://vuepress.vuejs.org/config/#styling)